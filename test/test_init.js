'use strict';

import process from 'node:process';
import { before } from 'mocha';
import chai from 'chai';
import dirtyChai from 'dirty-chai';

chai.use(dirtyChai);

before(function() {
  /* do not accept unhandledRejection */
  process.on('unhandledRejection', function(reason) {
    throw reason;
  });
});
