
import { expect } from 'chai';
import { describe, it } from 'mocha';
import CronRule from '../src/CronRule.js';

describe('CronRule', function() {
  it('can parse a cron rule', function() {
    const values = [
      { value: '42 0 * * *', result: { minute: 42, hour: 0, dayOfMonth: '*', month: '*', dayOfWeek: '*' } },
      { value: '*/25 * * * *', result: { minute: '*/25', hour: '*', dayOfMonth: '*', month: '*', dayOfWeek: '*' } }
    ];

    for (const test of values) {
      const rule = new CronRule(test.value);

      expect(rule).to.contain(test.result);
    }
  });

  it('can increment cron rules (strict)', function() {
    const values = [
      { value: '42 0 * * *', increment: '1 0 0 0 0', result: '43 0 * * *' },
      { value: '59 0 * * *', increment: '1 0 0 0 0', result: '0 1 * * *' },
      { value: '59 0 * * *', increment: '61 0 0 0 0', result: '0 2 * * *' },
      { value: '0 23 1 * *', increment: '0 1 0 0 0', result: '0 0 2 * *' },
      { value: '0 23 * * 7', increment: '0 25 0 0 0', result: '0 0 * * 2' }
    ];

    for (const test of values) {
      expect(new CronRule(test.value).increment(new CronRule(test.increment), true).toRule())
      .to.equal(test.result);
    }
  });

  it('fails on overflow (strict)', function() {
    /* CronRules will be incremented twice (to ensure increment syntax is valid) */
    const values = [
      { value: '58 * * * *', increment: '1 0 0 0 0' },
      { value: '58 23 * * *', increment: '1 0 0 0 0' }
    ];

    for (const test of values) {
      const rule = new CronRule(test.value);
      const incRule = new CronRule(test.increment);

      // first increment is valid
      rule.increment(incRule, true);

      expect(() => rule.increment(incRule, true))
      .to.throw(Error, 'failed',
        `should fail for "${test.value}".increment("${test.increment}")`);
    }
  });

  it('silently overflow on increment (not strict)', function() {
    const values = [
      { value: '59 * * * *', increment: '1 0 0 0 0', result: '0 * * * *' },
      { value: '59 23 * * *', increment: '1 0 0 0 0', result: '0 0 * * *' }
    ];

    for (const test of values) {
      const rule = new CronRule(test.value);
      const incRule = new CronRule(test.increment);

      expect(rule.increment(incRule).toRule()).to.equal(test.result);
    }
  });
});
