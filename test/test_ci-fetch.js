import { transform } from 'lodash-es';
import { expect } from 'chai';
import { afterEach, beforeEach, describe, it } from 'mocha';
import { ciFetch } from '../src/ci-fetch.js';
import Env from '../src/env.js';
import { stub } from 'sinon';

describe('ci-fetch', function() {
  let env;

  beforeEach(async function() {
    env = new Env();
    env.spin = transform([ 'start', 'succeed', 'fail', 'warn', 'info', 'debug', 'stop',
      'stopAndPersist', 'clear', 'render', 'frame' ], (ret, v) => { ret[v] = stub().returnsThis(); }, {});
    await env.load({ path: [], settings: [] },
      {
        url: 'https://gitlab.cern.ch',
        token: null,
        config: 'config',
        debug: false, dryRun: false, recurse: false,
        dumpProjects: false
      });

  });

  afterEach(function() {
    env?.spin?.stop();
    env.gitlab?.RepositoryFiles?.showRaw?.restore?.();
    env = null;
  });

  it('can parse references', async function() {
    const files = {
      '1234/.gitlab-ci.yml': `
.cxx-build:
  script:
    - echo world
build:
  extends: .cxx-deb-build
  script:
    - !reference [.cxx-build, script]`
    };

    stub(env.gitlab.RepositoryFiles, 'showRaw').callsFake((id, url) => {
      const content = files[`${id}/${url}`];
      if (content) {
        return content;
      }
      else {
        throw new Error(`unknown request: ${id} ${url}`);
      }
    });
    const ciFile = await ciFetch(env, { id: 1234 }, '.gitlab-ci.yml');
    /* a Reference object is created */
    expect(ciFile).to.have.nested.property('build.script[0]').to.be.a('Reference');
  });


  it('can process includes', async function() {
    const files = {
      '1234:.gitlab-ci.yml': `
include:
  - project: 'mro/common/tools/gitlab-ci-utils'
    file: 'ci/scripts/gitlab-ci-artifacts.yml'

build:
  extends: .cxx-deb-build`,

      '1235:ci/scripts/gitlab-ci-artifacts.yml': `
.cxx-deb-build:
  script:
    - echo cxx-deb-build
.cxx-build:
  script:
    - echo cxx-build`
    };

    stub(env.gitlab.RepositoryFiles, 'showRaw').callsFake((id, url) => {
      const content = files[`${id}:${url}`];
      if (content) {
        return content;
      }
      else {
        throw new Error(`unknown request: RespositoryFiles(${id}, ${url})`);
      }
    });
    stub(env.gitlab.Projects, 'show').callsFake((id) => {
      if (id === 'mro/common/tools/gitlab-ci-utils') {
        return { id: 1235 };
      }
      else {
        throw new Error(`unknown request: Projects(${id})`);
      }
    });
    const ciFile = await ciFetch(env, { id: 1234 }, '.gitlab-ci.yml');
    /* includes are resolved, but not `extends` */
    expect(ciFile).to.have.nested.property('\\.cxx-deb-build.script[0]', 'echo cxx-deb-build');
    expect(ciFile).to.have.property('build');
  });
});
