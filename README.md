# GitLab config

GitLab configuration scripts for EN-SMM-APC section.

## Installation

Assuming Node.js and npm are already installed:
```bash
npm install
```

An authentication token is required to run this tool:
```bash
# Using env
export ACCESS_TOKEN=xxx

# Using a file:
echo '"xxx"' > .token.json
```

## Running

To run the configuration scripts:
```bash
# Remove the dry-run part to actually update the configuration
./gitlab-config --dry-run --debug

# Some options are available:
./gitlab-config --help
```

## Debugging

This tool uses `gitlab-node` project, its cli can be used to debug and display GitLab configuration:
```bash
 npx gitlab --gl-host https://gitlab.cern.ch  groups projects --group-id 31123
```

## Adding configuration modules

Edit _config.js_ file to add or remove configuration modules.

Each module is an object in _settings_ attribute of the configuration, example:
```js
module.exports = {
  path: [
    'apc/common', /* path where to look for projects */
    'apc/experiments/ntof'
  ],
  settings: [
    { /* configuration module */
      plugin: 'mattermost', /* configuring mattermost */
      exclude: [ {} ], /* exclude all projects */
      include: [ { name: "gitlab-config" } ], /* force-include project named "gitlab-config" */
      config: {
        active: true, /* plugin dependant configuration values */
        ...
      }
    }
  ]
}
```

## Plugins

### Default parameters

Most plugins uses three parameters to filter associated projects : `include`, `exclude` and `excludeAll`.

The `excludeAll` parameter changes the way `include` and `exclude` are processed.

The `include` and `exclude` are list of objects that are compared with each
project's properties, when all properties in one of those object matches, the
rule is activated, ex:
```js
// All projects excluded, unless the ones named
// FileDisplay/DaqManager/ntofutils or docs
excludeAll: true,
include: [
  { name: 'FileDisplay' }, { name: 'DaqManager' },
  { name: 'ntofutils' }, { name: 'docs' }
],
```

When `excludeAll` is false or not set, all jobs are included by default, unless
matching the `exclude` rule but not the `include` rule.

When `excludeAll` is true, all jobs are excluded by default, unless matching the
`include` rule but not the `exclude` rule.

### gitlab-ci-badges

This plugin uses default parameters: `include`, `exclude` and `excludeAll`.

Configuration:
```js
{
  plugin: 'gitlab-ci-badges',
  config: {
    // optional coverage report link configuration
    coverage_link_url: 'https://apc-dev.web.cern.ch/ci/%{project_path}/coverage'
  }
  ...
}
```

### protected-tags

This plugin uses default parameters: `include`, `exclude` and `excludeAll`.

Configuration:
```js
{
  plugin: 'protected-tags',
  config: {
    tags: [
      { name: 'v*', access_level: 'maintainer' },
      { name: 'beta-*', access_level: 'developer' }
    ]
  }
  ...
}
```

### protected-branches

This plugin uses default parameters: `include`, `exclude` and `excludeAll`.

Configuration:
```js
{
  plugin: 'protected-branches',
  config: {
    branches: [
      { name: 'master', merge_access_level: 'maintainer', push_access_level: 'none', code_owner_approval_required: false }
    ]
  }
  ...
}
```

### deploy-keys

This plugin uses default parameters: `include`, `exclude` and `excludeAll`.

Configuration:
```js
{
  plugin: 'deploy-keys',
  config: {
    keys: [
      {
        title: 'apcdev-deploy-key',
        key: 'ssh-rsa  AAAAB3NzaC1yc2/...',
        can_push: true
      }
    ]
  }
}
```

### merge-request

This plugin uses default parameters: `include`, `exclude` and `excludeAll`.

For configuration details see:
- https://docs.gitlab.com/ee/api/projects.html#project-merge-method
- https://docs.gitlab.com/13.12/ee/api/merge_request_approvals.html

An additional _approvers_level_ argument has been added to update "Default"
approvers list.

Configuration:
```js
{
  plugin: 'merge-request',
  config: {
    approvers_level: 'maintainer', // update "Default" approvers rule
    merge_requests_access_level: 'enabled',
    only_allow_merge_if_pipeline_succeeds: true,
    only_allow_merge_if_all_discussions_are_resolved: true,
    merge_method: "rebase_merge", // "merge"|"rebase_merge"|"ff",
    printing_merge_request_link_enabled: true,
    resolve_outdated_diff_discussions: false,
    remove_source_branch_after_merge: true,
    approvals: {
      approvals_before_merge: 1,
      reset_approvals_on_push: true,
      disable_overriding_approvers_per_merge_request: true,
      merge_requests_author_approval: true
    }
  }
}
```
