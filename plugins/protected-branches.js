//@ts-check
import { every, findIndex, first, forEach, get, isEmpty, map, size,
  transform,
  upperFirst } from 'lodash-es';
import async from 'async';
import path from 'node:path';
import * as utils from '../src/utils.js';
import { AccessLevel } from '../src/Consts.js';
import { fileURLToPath } from 'node:url';
import { camelizeKeys } from 'xcase';

const service = path.basename(fileURLToPath(import.meta.url));

/**
 * @typedef {{
 *   name: string
 *   push_access_level?: GitConfig.$ProtectedBranchAccessLevel,
 *   merge_access_level?: GitConfig.$ProtectedBranchAccessLevel,
 *   code_owner_approval_required?: boolean
 * }} BranchConfig
 *
 * @typedef {{
 *   new: BranchConfig[],
 *   old: string[],
 *   update: BranchConfig[]
 * }} TagsMod
 */

/**
 * @param  {GitConfig.$AccessLevelSchema[]|undefined} project
 * @param  {GitConfig.$ProtectedBranchAccessLevel|undefined} level
 */
function checkLevels(project, level) {
  const levels = map(project, 'access_level');
  return (size(levels) <= 1) && first(levels) === level &&
    every(project, (p) => (p.user_id === null && p.group_id === null));
}

/**
 * @param  {GitConfig.$ProtectedBranchSchema[]} projectConf
 * @param  {Plugins.ProtectedBranchConfig[]} pluginConf
 * @return {Promise<TagsMod>}rs
 */
async function diffBranches(projectConf, pluginConf) {
  /** @type {TagsMod} */
  const ret = { new: [], old: [], update: [] };
  const conf = transform(pluginConf, (ret, c) => {
    ret.push({
      name: c.name,
      push_access_level: utils.getAccessLevel(c.push_access_level ?? /** @type {GitConfig.$ProtectedBranchAccessLevel} */ (AccessLevel.maintainer)),
      merge_access_level: utils.getAccessLevel(c.merge_access_level ?? /** @type {GitConfig.$ProtectedBranchAccessLevel} */ (AccessLevel.maintainer)),
      code_owner_approval_required: Boolean(c.code_owner_approval_required)
    });
  }, /** @type {BranchConfig[]} */ ([]));

  forEach(projectConf, (p) => {
    const idx = findIndex(conf, { name: p.name });
    if (idx >= 0) {
      const c = conf[idx];
      if (!checkLevels(p.push_access_levels, c.push_access_level) ||
          !checkLevels(p.merge_access_levels, c.merge_access_level) ||
          p.code_owner_approval_required !== c.code_owner_approval_required) {
        ret.update.push(c);
      }
      conf.splice(idx, 1);
    }
    else {
      ret.old.push(p.name);
    }
  });
  ret.new = conf;
  return ret;
}

/**
 * @param {GitConfig.Env} env
 * @param {GitConfig.$ProjectSchema} project
 * @param {GitConfig.Settings} settings
 * @details does not support user_id/group_id parameters (will remove it)
 */
export async function update(env, project, settings) {
  if (utils.isIgnored(env, project, settings, upperFirst(service))) {
    return;
  }

  env.spin.start(`Fetching ${service} configuration: ${project.name}`);
  var branches = /** @type {GitConfig.$ProtectedBranchSchema[]} */ (
    await env.gitlab.ProtectedBranches.all(project.id, {}));

  var mods = await diffBranches(branches, get(settings, 'config.branches'));

  env.spin.debug(`${upperFirst(service)} modifications: %o`, mods);
  if (env.opts.dryRun) {
    if (!isEmpty(mods.new)) {
      env.spin.warn(`${upperFirst(service)} addition required: ${project.name}`);
    }
    if (!isEmpty(mods.update)) {
      env.spin.warn(`${upperFirst(service)} update required: ${project.name}`);
    }
    if (!isEmpty(mods.old)) {
      env.spin.warn(`${upperFirst(service)} removal required: ${project.name}`);
    }
  }
  else {
    var updated = false;
    await async.eachSeries(mods.old, async function(name) {
      await env.gitlab.ProtectedBranches.unprotect(project.id, name);
      updated = true;
    });
    await async.eachSeries(mods.new, async function(s) {
      await env.gitlab.ProtectedBranches.protect(project.id, s.name,
        camelizeKeys(s));
      updated = true;
    });
    await async.eachSeries(mods.update, async function(s) {
      await env.gitlab.ProtectedBranches.unprotect(project.id, s.name);
      await env.gitlab.ProtectedBranches.protect(project.id, s.name,
        camelizeKeys(s));
      updated = true;
    });
    if (updated) {
      env.spin.succeed(`${upperFirst(service)} updated: ${project.name}`);
    }
  }
}

export default update;
