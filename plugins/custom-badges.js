//@ts-check
import { clone, find, first, get, includes, isEmpty, keys,
  unset, upperFirst } from 'lodash-es';
import * as utils from '../src/utils.js';
import axios from 'axios';

/**
 * @typedef {import('@gitbeaker/rest').ProjectBadgeSchema} ProjectBadgeSchema
 */

const service = 'custom-badges';

/**
 * @param  {Plugins.CustomBadgeConfig} badge
 * @param  {'image_url'|'link_url'} name
 * @param  {any} placeholders
 */
async function checkLink(badge, name, placeholders) {
  if (includes(badge.no_check, name)) {
    return;
  }
  return axios.get(utils.format(badge[name] || '', placeholders, false));
}

/**
 * @param  {GitConfig.Env} env
 * @param  {GitConfig.$ProjectSchema} project
 * @param  {any} placeholders
 * @param  {GitConfig.$BadgeSchema[]} projectBadges
 * @param  {{ [key:string]: Plugins.CustomBadgeConfig}} badges
 * @return {Promise<void>}
 */
async function checkBadges(env, project, placeholders, projectBadges, badges) {
  if (isEmpty(badges)) {
    return;
  }

  var name = /** @type {string} */ (first(keys(badges)));
  const msg = `${upperFirst(service)} ${name}`;
  var badge = badges[name];
  unset(badges, name);

  var current = find(projectBadges, { image_url: badge.image_url });

  env.spin.start(`Checking for badge ${name}: ${project.name}`);
  await Promise.all([
    checkLink(badge, 'image_url', placeholders),
    checkLink(badge, 'link_url', placeholders)
  ])
  .then(
    async () => {
      if (current && (current.link_url === badge.link_url)) {
        env.spin.debug(`${msg} already displayed: ${project.name}`);
      }
      else if (env.opts.dryRun) {
        env.spin.warn(`${msg} update required: ${project.name}`);
      }
      else if (current) {
        await env.gitlab.ProjectBadges.edit(project.id, current.id,
          { imageUrl: badge.image_url, linkUrl: badge.link_url });
        env.spin.succeed(`${msg} updated: ${project.name}`);
      }
      else {
        await env.gitlab.ProjectBadges.add(project.id, badge.link_url, badge.image_url);
        env.spin.succeed(`${msg} added: ${project.name}`);
      }
    },
    async (err) => {
      if (current) {
        if (env.opts.dryRun) {
          env.spin.warn(`${msg} removal required: ${project.name}`);
        }
        else {
          await env.gitlab.ProjectBadges.remove(project.id, current.id);
          env.spin.succeed(`${msg} removed: ${project.name}`);
        }
      }
      else {
        env.spin.debug(`${msg} not needed: ${project.name} (${err})`);
      }
    });
  return checkBadges(env, project, placeholders, projectBadges, badges);
}

/**
 * @param  {GitConfig.Env} env
 * @param  {GitConfig.$ProjectSchema} project
 * @param  {GitConfig.Settings} settings
 */
async function update(env, project, settings) {
  if (utils.isIgnored(env, project, settings, upperFirst(service))) {
    return;
  }

  var placeholders = {
    project_path: project.path_with_namespace,
    project_id: project.id,
    commit_sha: project.default_branch,
    default_branch: project.default_branch
  };
  env.spin.start(`Fetching pipeline-badge config: ${project.name}`);
  const badges = /** @type {ProjectBadgeSchema[]} */ (await env.gitlab.ProjectBadges.all(project.id));
  return checkBadges(env, project, placeholders, badges, clone(get(settings, 'config.badges')) || []);
}

export default update;
