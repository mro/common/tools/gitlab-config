//@ts-check
import { cloneDeep, every, filter, forEach, get, isEmpty, isNil, size,
  some, upperFirst } from 'lodash-es';
import async from 'async';
import path from 'node:path';
import * as utils from '../src/utils.js';
import { fileURLToPath } from 'node:url';

const service = path.basename(fileURLToPath(import.meta.url));

/**
 * @typedef {{
    new: Plugins.ProtectedResourceConfig[],
    old: string[],
    update: { [key:string]: Plugins.ProtectedResourceConfig[] }
  }} TagsMod
*/

/**
 * @param  {GitConfig.Env} env
 * @param  {GitConfig.$ProtectedResourceSchema[]} projectTags
 * @param  {Plugins.ProtectedResourceConfig[]} confTags
 * @return {Promise<TagsMod>}
 */
async function diffTags(env, projectTags, confTags) {
  /** @type {TagsMod} */
  const ret = { new: [], old: [], update: {} };
  confTags = cloneDeep(confTags);
  await async.eachSeries(confTags, async function(c) {
    // @ts-ignore
    c.access_level = utils.getAccessLevel(c.access_level);
    // @ts-ignore
    c.user = await utils.fetchUserId(env.gitlab, c.user);
  });

  forEach(projectTags, (p) => {
    /** @type {Plugins.ProtectedResourceConfig[]} */
    const wantedTags = [];
    confTags = filter(confTags, (c) => {
      if (p.name === c.name) {
        wantedTags.push(c);
        return false;
      }
      return true;
    });

    if (isEmpty(wantedTags)) {
      ret.old.push(p.name);
    }
    else {
      const update = (size(wantedTags) !== size(p.create_access_levels)) ||
        !every(p.create_access_levels, (level) => {
          if (!isNil(level.access_level)) {
            return some(wantedTags, { access_level: level.access_level });
          }
          else if (!isNil(level.user_id)) {
            return some(wantedTags, { user: level.user_id });
          }
          return false;
        });
      if (update) {
        ret.update[p.name] = wantedTags;
      }
    }
  });
  ret.new = confTags;
  return ret;
}

/**
 * @param  {Plugins.ProtectedResourceConfig} c
 */
function confToParam(c) {
  if (c.user_id) {
    // check gitlab/lib/api/protected_tags.rb in gitlab sources
    throw 'User protected tags not (yet) supported by GitLab REST API';
    // return { create_user_id: c.user };
  }
  else if (c.access_level) {
    return { createAccessLevel: /** @type {import('@gitbeaker/rest').ProtectedTagAccessLevel} */ (c.access_level) };
  }
  return undefined;
}


/**
 * @param {GitConfig.Env} env
 * @param {GitConfig.$ProjectSchema} project
 * @param {GitConfig.Settings} settings
 */
export async function update(env, project, settings) {
  if (utils.isIgnored(env, project, settings, upperFirst(service))) {
    return;
  }

  env.spin.start(`Fetching ${service} configuration: ${project.name}`);
  var tags = /** @type {GitConfig.$ProtectedResourceSchema[]} */ (
    await env.gitlab.ProtectedTags.all(project.id, {}));

  var mods = await diffTags(env, tags, get(settings, 'config.tags'));

  env.spin.debug(`${upperFirst(service)} modifications: %o`, mods);
  if (env.opts.dryRun) {
    if (!isEmpty(mods.new)) {
      env.spin.warn(`${upperFirst(service)} addition required: ${project.name}`);
    }
    if (!isEmpty(mods.update)) {
      env.spin.warn(`${upperFirst(service)} update required: ${project.name}`);
    }
    if (!isEmpty(mods.old)) {
      env.spin.warn(`${upperFirst(service)} removal required: ${project.name}`);
    }
  }
  else {
    var updated = false;
    await async.eachSeries(mods.new, async function(s) {
      await env.gitlab.ProtectedTags.protect(project.id, s.name, confToParam(s));
      updated = true;
    });
    await async.eachOfSeries(mods.update, async function(update, name) {
      await env.gitlab.ProtectedTags.unprotect(project.id, /** @type {string} */ (name));
      await async.eachSeries(update, async function(s) {
        await env.gitlab.ProtectedTags.protect(project.id, /** @type {string} */ (name), confToParam(s));
      });
      updated = true;
    });
    await async.eachSeries(mods.old, async function(name) {
      await env.gitlab.ProtectedTags.unprotect(project.id, name);
      updated = true;
    });
    if (updated) {
      env.spin.succeed(`${upperFirst(service)} updated: ${project.name}`);
    }
  }
}

export default update;
