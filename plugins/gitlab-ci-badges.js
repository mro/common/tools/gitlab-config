//@ts-check
import { clone, find, get, has, some, startsWith, upperFirst } from 'lodash-es';
import * as utils from '../src/utils.js';
import axios from 'axios';
import ciFetch from '../src/ci-fetch.js';

/**
 * @typedef {{ link_url: string, image_url: string }} CIBadge
 * @typedef {import('@gitbeaker/core').ProjectBadgeSchema} ProjectBadgeSchema
 */

const serviceName = upperFirst('gitlab-ci-badges');

const pipeline_badge = {
  image_url: 'https://gitlab.cern.ch/%{project_path}/badges/%{default_branch}/pipeline.svg',
  link_url: 'https://gitlab.cern.ch/%{project_path}/pipelines'
};
const coverage_badge = {
  link_url: 'https://apc-dev.web.cern.ch/ci/%{project_path}/coverage',
  image_url: 'https://gitlab.cern.ch/%{project_path}/badges/%{default_branch}/coverage.svg'
};

/**
 * @param  {string} url
 * @param  {any} placeholders
 */
async function checkLink(url, placeholders /*: {} */) {
  return axios.get(utils.format(url, placeholders, false));
}

/**
 * @param  {GitConfig.Env} env
 * @param  {GitConfig.$ProjectSchema} project
 * @param  {GitConfig.$BadgeSchema[]} badges
 * @param  {CIBadge} ci_badge
 * @param  {string} name
  */
async function update_badge(env, project, badges, ci_badge, name) {
  const current = find(badges, { image_url: ci_badge.image_url });
  if (current && (current.link_url === ci_badge.link_url)) {
    env.spin.debug('%s %s already displayed: %s', serviceName, name, project.name);
  }
  else if (env.opts.dryRun) {
    env.spin.warn('%s %s update required: %s', serviceName, name, project.name);
  }
  else {
    if (current) {
      await env.gitlab.ProjectBadges.edit(project.id, current.id,
        { linkUrl: ci_badge.link_url, imageUrl: ci_badge.image_url, name });
    }
    else {
      await env.gitlab.ProjectBadges.add(project.id, ci_badge.link_url,
        ci_badge.image_url, { name });
    }
    env.spin.succeed('%s %s updated: %s', serviceName, name, project.name);
  }
}

/**
 * @param {GitConfig.Env} env
 * @param {GitConfig.$ProjectSchema} project
 * @param  {GitConfig.Settings} settings
 * @param {{[key:string]: any}} gitlabCi
 */
async function checkBadges(env, project, settings, gitlabCi) {
  env.spin.start(`Fetching pipeline-badge config: ${project.name}`);
  const badges = /** @type {ProjectBadgeSchema[]} */ (await env.gitlab.ProjectBadges.all(project.id));
  await update_badge(env, project, badges, pipeline_badge, 'pipeline-badge');

  if (some(gitlabCi, (/** @type {string} */ job, /** @type {any} */ name) => (!startsWith(name, '.') && has(job, 'coverage')))) {
    const badge = clone(coverage_badge);
    const placeholders = {
      project_path: project.path_with_namespace,
      project_id: project.id,
      commit_sha: project.default_branch,
      default_branch: project.default_branch
    };
    badge.link_url = get(settings, [ 'config', 'coverage_link_url' ], badge.link_url);

    await checkLink(badge.link_url, placeholders)
    .catch(() => {
      env.spin.debug('%s using default coverage url: %s', serviceName, project.name);
      badge.link_url = 'https://gitlab.cern.ch/%{project_path}';
    });
    await update_badge(env, project, badges, badge, 'coverage-badge');
  }
  else {
    env.spin.debug(`${serviceName} no coverage info: ${project.name}`);
  }
}

/**
 * @param  {GitConfig.Env} env
 * @param  {GitConfig.$ProjectSchema} project
 * @param  {GitConfig.Settings} settings
 */
export async function update(env, project, settings) {
  if (utils.isIgnored(env, project, settings, serviceName)) {
    return;
  }

  env.spin.start(`Fetching gitlab-ci file: ${project.name}`);

  await ciFetch(env, project)
  .then(
    (gitlabCi) => checkBadges(env, project, settings, gitlabCi),
    () => env.spin.debug('%s no gitlab-ci file: %s', serviceName, project.name));
}

export default update;
