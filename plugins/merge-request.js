// @ts-check
import { filter, get, includes, isEmpty, isEqual, isNil,
  keyBy, map, omitBy, upperFirst } from 'lodash-es';
import path from 'path';
import * as utils from '../src/utils.js';
import { AccessLevel } from  '../src/Consts.js';
import { fileURLToPath } from 'node:url';

const service = path.basename(fileURLToPath(import.meta.url));

/**
 * @typedef {import('@gitbeaker/rest').MemberSchema} MemberSchema
 * @typedef {import('@gitbeaker/rest').ProjectLevelApprovalRuleSchema} ProjectLevelApprovalRuleSchema
 */

/**
 * @param {GitConfig.Env} env
 * @param {GitConfig.$ProjectSchema} project
 * @param {GitConfig.Settings} settings
 */
async function approval_rules(env, project, settings) {
  /** @type {keyof typeof AccessLevel} */
  const level_name = get(settings, 'config.approvers_level');
  if (isNil(level_name)) { return; }

  const level = AccessLevel[level_name];

  const members = map(
    filter(/** @type {MemberSchema[]} */ (await env.gitlab.ProjectMembers.all(project.id, { includeInherited: true })),
      (m) => m.access_level >= level),
    (m) => m.id).sort();

  const rules = keyBy(await env.gitlab.MergeRequestApprovals.allApprovalRules(project.id), 'name');
  const rule = /** @type {ProjectLevelApprovalRuleSchema} */ (rules['Default']);

  if (isNil(rule)) {
    if (env.opts.dryRun) {
      env.spin.warn(`${upperFirst(service)} approval-rule addition required: ${project.name}`);
    }
    else {
      env.spin.start(`Adding approval-rule for merge-requests: ${project.name}`);
      await env.gitlab.MergeRequestApprovals.createApprovalRule(project.id, 'Default', 1, {
        userIds: members
      });
    }
  }
  else {
    const currentMembers = map(rule.users, 'id').sort();
    if (!isEqual(members, currentMembers)) {
      if (env.opts.dryRun) {
        env.spin.warn(`${upperFirst(service)} approval-rule update required: ${project.name}`);
      }
      else {
        env.spin.start(`Updating approval-rule for merge-requests: ${project.name}`);
        await env.gitlab.MergeRequestApprovals.editApprovalRule(project.id,
          rule.id, rule.name, rule.approvals_required, { userIds: members });
      }
    }
    else if (env.opts.dryRun) {
      env.spin.debug(`${upperFirst(service)} approval-rule up-to-date: ${project.name}`);
      return false;
    }
    else {
      return false;
    }
  }
  return true;
}

/**
 * @param {GitConfig.Env} env
 * @param {GitConfig.$ProjectSchema} project
 * @param {GitConfig.Settings} settings
 */
export async function update(env, project, settings) { /* eslint-disable-line complexity, max-statements */
  if (utils.isIgnored(env, project, settings, upperFirst(service))) {
    return;
  }

  /** @type {GitConfig.$UserSchema[]|undefined} */
  env.spin.start(`Fetching merge-requests configuration: ${project.name}`);

  /** @type {GitConfig.$MergeApprovalSchema} */ // @ts-ignore
  const current = await env.gitlab.MergeRequestApprovals.showConfiguration(project.id, {});
  var config = omitBy(settings.config, (value, key) => {
    return includes([ 'approvers', 'approvals', 'approvers_level' ], key) ||
      isEqual(value, get(project, [ key ]));
  });
  const rulesUpdated = await approval_rules(env, project, settings);
  var approvals = omitBy(get(settings, 'config.approvals'),
    (value, key /*: string */) => isEqual(value, get(current, [ key ])));

  if (isEmpty(config) && isEmpty(approvals) && !rulesUpdated) {
    env.spin.debug(`${upperFirst(service)} already up-to-date: ${project.name}`);
    return;
  }
  env.spin.debug('%s config diff: %o', upperFirst(service), config);
  env.spin.debug('%s approvals diff: %o', upperFirst(service), approvals);
  if (env.opts.dryRun) {
    if (!isEmpty(config)) {
      env.spin.warn(`${upperFirst(service)} configuration update required: ${project.name}`);
    }
    if (!isEmpty(approvals)) {
      env.spin.warn(`${upperFirst(service)} approvals update required: ${project.name}`);
    }
  }
  else {
    if (!isEmpty(config)) {
      env.spin.start(`Updating merge-requests configuration: ${project.name}`);
      await env.gitlab.Projects.edit(project.id, config);
    }
    if (!isEmpty(approvals)) {
      env.spin.start(`Updating merge-requests approvals: ${project.name}`);
      // @ts-ignore
      await env.gitlab.MergeRequestApprovals.editConfiguration(project.id, approvals);
    }
    env.spin.succeed(`${upperFirst(service)} updated: ${project.name}`);
  }
}

export default update;
