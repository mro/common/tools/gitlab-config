//@ts-check

import { assign, get, isEmpty, isEqual, isNil, omitBy,
  upperFirst } from 'lodash-es';
import * as utils from '../src/utils.js';

/**
 * @typedef {import('@gitbeaker/core').SupportedIntegration} SupportedIntegration
 */

/**
 * @param {GitConfig.Env} env
 * @param {GitConfig.$ProjectSchema} project
 * @param {GitConfig.Settings} settings
 * @param {SupportedIntegration} service
 */
export async function update(env, project, settings, service) {
  if (isNil(service)) {
    env.spin.debug(`No service provided on simple-update plugin: ${project.name}`);
    return;
  }
  if (utils.isIgnored(env, project, settings, upperFirst(service))) {
    return;
  }

  env.spin.start(`Fetching ${service} config: ${project.name}`);
  let current = await env.gitlab.Integrations.show(project.id, service)
  .catch(() => {
    // GitLab bug on 14.4 returns 404 when integration is not active
    env.spin.warn(`${upperFirst(service)} service not active: ${project.name}`);
    return {};
  });
  current = assign(current, get(current, 'properties'));
  env.spin.debug(`%o <-> %o`, settings.config, current);
  const updates = omitBy(settings.config,
    (value, key) => isEqual(value, get(current, key)));

  if (isEmpty(updates)) {
    env.spin.debug(`${upperFirst(service)} already up-to-date: ${project.name}`);
    return;
  }
  env.spin.debug('%s diff: %o', upperFirst(service), updates);
  if (env.opts.dryRun) {
    env.spin.warn(`${upperFirst(service)} update required: ${project.name}`);
  }
  else {
    await env.gitlab.Integrations.edit(project.id, service, settings.config);
    env.spin.succeed(`${upperFirst(service)} updated: ${project.name}`);
  }
}

export default update;
