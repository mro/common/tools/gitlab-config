//@ts-check
import { clone, cloneDeep, filter, findIndex, forEach, get, isEmpty, isEqual,
  set, transform, unset, upperFirst } from 'lodash-es';
import async from 'async';
import * as utils from '../src/utils.js';
import CronRule from '../src/CronRule.js';

const service = 'schedules';

/**
 * @typedef {{
 *   new: Plugins.ScheduleConfig[],
 *   old: GitConfig.$PipelineScheduleSchema[],
 *   update: GitConfig.$PipelineScheduleSchema[]
 * }} PipelineMod
 */

const s_filteredConfig = [ 'increment' ];

/**
 * @param  {string} prefix
 * @param  {GitConfig.$PipelineScheduleSchema[]} projectSchedules
 * @param  {Plugins.ScheduleConfig[]} confSchedules
 * @return {PipelineMod}
 */
function diffSchedules(prefix, projectSchedules, confSchedules) {
  /** @type {PipelineMod} */
  const ret = { new: [], old: [], update: [] };
  confSchedules = cloneDeep(confSchedules);
  forEach(confSchedules, (c) => {
    c.description = prefix + c.description;
    forEach(s_filteredConfig, (name) => unset(c, name));
  });

  forEach(projectSchedules, (p) => {
    var index = findIndex(confSchedules, { description: p.description });
    if (index === -1) {
      ret.old.push(clone(p));
    }
    else {
      var conf = confSchedules[index];
      confSchedules.splice(index, 1);
      var update = transform(conf,
        (ret, value /*: any */, prop /*: string */) => {
          if (!isEqual(get(p, [ prop ]), value)) {
            set(ret, [ prop ], value);
          }
        }, /** @type {GitConfig.$PipelineScheduleSchema} */ ({}));
      if (!isEmpty(update)) {
        update.id = p.id;
        /* always update description */
        update.description = conf.description || '';
        ret.update.push(update);
      }
    }
  });
  ret.new = confSchedules;
  return ret;
}

/**
 * @brief update schedule
 * @param {Plugins.ScheduleConfig} schedule
 * @details schedules may need update after use
 */
async function updateSchedule(schedule) {
  const increment = get(schedule, 'increment');
  if (increment) {
    const cronRule = new CronRule(get(schedule, 'cron'));
    const incRule = new CronRule(increment);

    schedule.cron = cronRule.increment(incRule).toRule();
  }
}

/**
 * @param {GitConfig.Env} env
 * @param {GitConfig.$ProjectSchema} project
 * @param {GitConfig.Settings} settings
 */
export async function update(env, project, settings) {
  if (utils.isIgnored(env, project, settings, upperFirst(service))) {
    return;
  }

  env.spin.start(`Fetching ${service} config: ${project.name}`);
  var projectSchedules = /** @type {GitConfig.$PipelineScheduleSchema[]} */ (
    await env.gitlab.PipelineSchedules.all(project.id));

  const prefix = get(settings, 'config.prefix', '[auto] ');
  projectSchedules = filter(projectSchedules, (s) => s.description.startsWith(prefix));

  var mods = diffSchedules(prefix, projectSchedules,
    get(settings, 'config.schedules'));

  for (const schedule of get(settings, 'config.schedules')) {
    await updateSchedule(schedule).catch(
      () => env.spin.fail(`${upperFirst(service)} failed to update schedule: ${project.name}`)); // jshint ignore:line
  }

  env.spin.debug(`${upperFirst(service)} modifications: %o`, mods);
  if (env.opts.dryRun) {
    if (!isEmpty(mods.new)) {
      env.spin.warn(`${upperFirst(service)} addition required: ${project.name}`);
    }
    if (!isEmpty(mods.update)) {
      env.spin.warn(`${upperFirst(service)} update required: ${project.name}`);
    }
    if (!isEmpty(mods.old)) {
      env.spin.warn(`${upperFirst(service)} removal required: ${project.name}`);
    }
  }
  else {
    var updated = false;
    await async.eachSeries(mods.new, async function(s) {
      await env.gitlab.PipelineSchedules.create(project.id, s.description, s.ref,
        s.cron, s);
      updated = true;
    });
    await async.eachSeries(mods.update, async function(s) {
      await env.gitlab.PipelineSchedules.edit(project.id, s.id, s);
      updated = true;
    });
    await async.eachSeries(mods.old, async function(s) {
      await env.gitlab.PipelineSchedules.remove(project.id, s.id);
      updated = true;
    });
    if (updated) {
      env.spin.succeed(`${upperFirst(service)} updated: ${project.name}`);
    }
  }
}

export default update;
