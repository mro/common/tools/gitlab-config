
import { $ResourceId } from '../src/types';

export = Plugins
export as namespace Plugins

declare namespace Plugins {
  interface CustomBadgeConfig {
    image_url: string,
    link_url: string,
    no_check?: string[],
    id?: $ResourceId
  }

  interface DeployKeyConfig {
    title: string,
    key: string,
    can_push?: boolean
  }

  interface ProtectedResourceConfig {
    name: string,
    access_level?: string | GitConfig.$AccessLevel,
    user_id?: string | number,
    group_id?: string | number
  }

  interface ScheduleConfig {
    description: string,
    ref: string,
    cron: string,
    cron_timezone: string,
    active: boolean,
    increment?: string
  }

  type ProtectedBranchConfig = {
    name: string
    push_access_level?: string | GitConfig.$ProtectedBranchAccessLevel,
    merge_access_level?: string | GitConfig.$ProtectedBranchAccessLevel,
    code_owner_approval_required?: boolean
  }
}