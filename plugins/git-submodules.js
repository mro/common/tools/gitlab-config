//@ts-check
import { some, upperFirst } from 'lodash-es';
import path from 'node:path';
import * as utils from '../src/utils.js';
import { fileURLToPath } from 'node:url';

const service = path.basename(fileURLToPath(import.meta.url));

/**
 * @param  {GitConfig.Env} env
 * @param  {GitConfig.$ProjectSchema} project
 * @param  {GitConfig.Settings} settings
 */
export async function update(env, project, settings) {
  if (utils.isIgnored(env, project, settings, upperFirst(service))) {
    return;
  }

  env.spin.start(`Fetching .gitmodules file: ${project.name}`);
  return await env.gitlab.RepositoryFiles.showRaw(project.id, '.gitmodules', project.default_branch)
  .then(
    async () => {
      const vars = await env.gitlab.ProjectVariables.all(project.id);
      if (some(vars, { key: 'GIT_SUBMODULE_STRATEGY', value: 'recursive' })) {
        env.spin.debug(`${upperFirst(service)} already up-to-date: ${project.name}`);
      }
      else if (env.opts.dryRun) {
        env.spin.warn(`${upperFirst(service)} update required: ${project.name}`);
      }
      else {
        await env.gitlab.ProjectVariables.create(project.id, 'GIT_SUBMODULE_STRATEGY', 'recursive', {
          variableType: 'env_var'
        });
        env.spin.succeed(`${upperFirst(service)} updated: ${project.name}`);
      }
    },
    () => env.spin.debug(`${upperFirst(service)} no submodules: ${project.name}`)
  );
}

export default update;
