//@ts-check

import { get, isMatchWith, isNil, isObject, noop, partial, replace, some } from 'lodash-es';
import { AccessLevel } from './Consts.js';
import path from 'node:path';
import fs from 'node:fs/promises';
import yaml from 'js-yaml';

/**
 * @typedef {import('@gitbeaker/rest')['Gitlab']} Gitlab
 */

/**
 * @param  {any} object
 * @param  {any} source
 * @return {boolean}
 */
function deepMatch(object, source) {
  return isMatchWith(object, source, function(objValue, srcValue) {
    if (isObject(objValue) && isObject(srcValue)) {
      return deepMatch(objValue, srcValue);
    }
  });
}

/**
 * format a string
 * @details replaces any "%{words}" from it's value repl map.
 * @param {string} string
 * @param {{ [key:string]: any }} repl
 * @param  {boolean} keep  activate to strip unknown "{words}"
 */
export function format(string, repl, keep) {
  return replace(string, /%{([^\}]*)}/g, function(match, key) {
    return get(repl, key, keep ? match : '');
  });
}

/**
 * @param  {GitConfig.Env}  env
 * @param  {GitConfig.$ProjectSchema}  project
 * @param  {GitConfig.Filter}  config
 * @param  {string}  name
 */
export function isIgnored(env, project, config, name) {
  const excludeAll = get(config, 'excludeAll', false);
  const include = some(config.include, partial(deepMatch, project));
  const exclude = some(config.exclude, partial(deepMatch, project));

  if (excludeAll) {
    if (include && !exclude) {
      env.spin.debug(`${name} force-include: ${project.name}`);
      return false;
    }
    env.spin.debug(`${name} skipping: ${project.name}`);
    return true;
  }
  else {
    if (include) {
      env.spin.debug(`${name} force-include: ${project.name}`);
      return false;
    }
    else if (exclude) {
      env.spin.debug(`${name} skipping: ${project.name}`);
      return true;
    }
    return false;
  }
}

/**
 * @template [T=GitConfig.$AccessLevel]
 * @param  {string|T} name
 * @return {T}
 */
export function getAccessLevel(name) {
  // @ts-ignore
  return get(AccessLevel, name, name);
}

/** @type {{ [key:string]: number }} */
const userMap = {};

/**
 * @param {InstanceType<Gitlab>} gitlab
 * @param {string} name
 * @return {Promise<number>}
 */
export async function fetchUserId(gitlab, name) {
  if (isNil(name)) {
    /* $FlowIgnore */
    return name;
  }
  if (!userMap[name]) {
    const user = await gitlab.Users.all({ username: name });
    userMap[name] = get(user, [ 0, 'id' ]);
  }
  return userMap[name];
}

/**
 *
 * @param {string} filename
 * @returns {Promise<string|null>}
 */
async function resolveFile(filename) {
  try {
    await fs.access(filename, fs.constants.R_OK);
    return filename;
  }
  catch {
    noop();
  }

  for (const ext of [ '.js', '.json', '.yaml', '.yml' ]) {
    try {
      const out = filename + ext;
      await fs.access(out, fs.constants.R_OK);
      return out;
    }
    catch {
      continue;
    }
  }
  return null;
}

/**
 * @param {string} filename
 */
export async function loadFile(filename) {
  const file = await resolveFile(path.resolve(filename));
  if (!file) {
    throw new Error(`No such file: ${filename}`);
  }
  else if (file.endsWith('.js')) {
    return (await import(file))?.default;
  }
  else {
    return yaml.load(await fs.readFile(file, 'utf8'));
  }
}

