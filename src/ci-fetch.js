//@ts-check
import { assign, clone, get, has, isString, map, mapValues, omitBy, set,
  startsWith, unset } from 'lodash-es';
import axios from 'axios';
import yaml from 'js-yaml';

var globalCache = {};

/* parse additional gitlab types: !reference ["..."] */
class Reference {
  /**
   * @param {string[]} ref
   */
  constructor(ref) {
    this.ref = ref;
  }

  get [Symbol.toStringTag]() {return  'Reference'; }
}

const ReferenceType = new yaml.Type('!reference', {
  kind: 'sequence',
  construct(data) { return new Reference(data); },
  instanceOf: Reference
});
const schema = yaml.DEFAULT_SCHEMA.extend(ReferenceType);

/**
 * @param {any} config
 * @param {string} name
 */
function ciResolve(config, name) {
  var job = get(config, name);
  if (has(job, 'extends') && has(config, job.extends)) {
    job = assign({}, ciResolve(config, job.extends), job);
    unset(job, 'extends');
    config[name] = job;
  }
  return job;
}

/**
 * @param {GitConfig.Env} env
 * @param {GitConfig.$ProjectSchema} project
 * @param {string} url
 * @param {{ [key:string]: any }} cache
 * @details local or remote includes
 */
async function ciFetchSingle(env, project, url, cache) {
  var ret;
  if (has(cache, [ url ])) {
    return cache[url];
  }
  else if (startsWith(url, 'http')) {
    ret = yaml.load((await axios.get(url)).data, { schema });
  }
  else {
    // type issue in gitbeaker typescript
    /** @type {string} */
    ret = yaml.load(/** @type {any} */ (await env.gitlab.RepositoryFiles.showRaw(
      project.id, url, get(project, 'default_branch'))), { schema });
  }

  const includes = await Promise.all(
    map(get(ret, 'include'), (inc) => ciFetch(env, project, inc, cache)));

  /** @type {any} */
  ret = assign({}, ...includes, ret);
  unset(ret, 'include'); /* now resolved */
  set(cache, [ url ], ret);
  return ret;
}

/**
 * @param {GitConfig.Env} env
 * @param {GitConfig.$ProjectSchema} project
 * @param {{ file: string|string[], project?: string, ref?: string }} inc
 * @param {{ [key:string]: any }} cache
 */
async function ciFetchProjects(env, project, inc, cache) {
  const file = inc.file;
  const path = /** @type {string|number} */ (inc.project);
  const ref = inc.ref ?? 'HEAD';

  const remoteProject = /** @type {GitConfig.$ProjectSchema} */
    (await env.gitlab.Projects.show(path));

  const files = await Promise.all(map(isString(file) ? [ file ] : file, async (file) => {
    const uri = `project://${remoteProject.id}:${file}@${ref}`;
    if (has(cache, [ uri ])) {
      return cache[uri];
    }
    const raw = /** @type {any} */ (await env.gitlab.RepositoryFiles.showRaw(
      remoteProject.id, file, ref));
    var ret = yaml.load(raw, { schema });
    const includes = await Promise.all(map(get(ret, 'include'),
      (inc) => ciFetch(env, remoteProject, inc, cache)));
    ret = assign({}, ...includes, ret);
    unset(ret, 'include'); /* now resolved */
    set(cache, [ uri ], ret);
    return ret;
  }));
  return assign({}, ...files);
}

/**
 * @param {GitConfig.Env} env
 * @param {GitConfig.$ProjectSchema} project
 * @param {string} url
 * @param {{ [key:string]: any }} cache
 * @return {Promise<{ [key:string]: any }>}
 */
export async function ciFetch(env, project, url, cache) {
  /** @type {any[]} */
  if (!isString(url)) {
    if (has(url, 'remote')) {
      return ciFetchSingle(env, project, get(url, 'remote'), cache);
    }
    else if (has(url, 'local')) {
      return ciFetchSingle(env, project, get(url, 'local'), cache);
    }
    else if (has(url, 'project')) {
      return ciFetchProjects(env, project, url, cache);
    }
    else {
      env.spin.warn(`unsupported include in gitlab-ci file: ${project.name}`);
      return {};
    }
  }
  else {
    return ciFetchSingle(env, project, url, cache);
  }
}

/**
 * @param {GitConfig.Env} env
 * @param {GitConfig.$ProjectSchema} project
 */
async function loadGitlabCi(env, project) {
  var localCache = clone(globalCache);
  return ciFetch(env, project, '.gitlab-ci.yml', localCache)
  .then((config) => mapValues(config, (v, name) => ciResolve(config, name)))
  .then((ret) => {
    globalCache = omitBy(localCache,
      (v, k) => !startsWith(k, 'http') && !startsWith(k, 'project'));
    return ret;
  });
}

export default loadGitlabCi;
