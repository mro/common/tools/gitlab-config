//@ts-check
import { isEmpty, set } from 'lodash-es';
import fs from 'node:fs/promises';
import process from 'node:process';
import path from 'node:path';
import ora from './ora-ext.js';
import { Gitlab } from '@gitbeaker/rest';
import { loadFile } from './utils.js';
import { fileURLToPath } from 'node:url';

const dirname = path.dirname(fileURLToPath(import.meta.url));

class Env {
  constructor() {
    /** @type {GitConfig.Config} */ // @ts-ignore
    this.config = null;
    /** @type {GitConfig.EnvOpts} */ // @ts-ignore
    this.opts = null;
    /** @type {OraExt.$OraExt} */ // @ts-ignore
    this.spin = null;
    /** @type {InstanceType<Gitlab<false>>} */ // @ts-ignore
    this.gitlab = null;
    /** @type {{ [key:string]: GitConfig.Plugin }} */
    this.plugins = {};
  }

  /**
   * @param {GitConfig.Config|string} config
   * @param {GitConfig.EnvOpts} opts
   */
  // eslint-disable-next-line complexity
  async load(config, opts) {
    this.opts = opts;

    if (typeof config === 'string') {
      this.config = await loadFile(config)
      .catch((err) => {
        console.log(err);
        throw new Error(`Configuration file not found: ${config}`);
      });
    }
    else {
      this.config = config;
    }

    /* override path from env */
    if (this.opts.path && !isEmpty(this.opts.path)) {
      this.config.path = this.opts.path;
    }

    /* null token means no token */
    if (this.opts.token === undefined) {
      this.opts.token = process.env['ACCESS_TOKEN'] ?? await loadFile(
        path.join(process.cwd(), '.token'))
      .catch((err) => {
        console.log(err);
        throw new Error(`Access token is required`);
      });
    }

    // @ts-ignore
    this.gitlab = new Gitlab({ host: this.opts.url, token: this.opts.token });
    if (!this.spin) {
      this.spin = ora('Loading...').start();
      set(this.spin, 'options.debug', this.opts.debug);
    }

    const pluginDir = path.join(dirname, '..', 'plugins');

    this.plugins = {};
    for (const file of await fs.readdir(pluginDir)) {
      if (!file.endsWith('.js')) { continue; }
      this.spin.debug('Loading plugin %s', file);
      var info = path.parse(file);
      this.plugins[info.name] = (await import(path.join(pluginDir, file)))?.default;
    }
  }
}

export default Env;
