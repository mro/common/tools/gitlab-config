//@ts-check
import { concat, filter, forEach, get, map } from 'lodash-es';
import { Command, InvalidOptionArgumentError } from 'commander';
import process from 'node:process';
import path from 'node:path';
import Env from './env.js';
import { isIgnored } from  '../src/utils.js';

/**
 * @typedef {import('@gitbeaker/core').GroupSchema} GroupSchema
 */

const cmd = new Command();

/**
 * @param  {any} val
 * @param  {any[]} memo
 */
function collect(val, memo) {
  memo.push(val);
  return memo;
}

/**
 * @param  {string} val
 * @param  {any} memo
 */
function jsonCollect(val, memo) {
  try {
    memo.push(JSON.parse(val));
    return memo;
  }
  catch (e) {
    throw new InvalidOptionArgumentError('Failed to parse JSon argument.');
  }
}

cmd
.option('-u, --url <GitLab url>', 'GitLab url', 'https://gitlab.cern.ch')
.option('-p, --path <path>', 'Projects path in GitLab', collect, [ ])
.option('--plugin <plugin>', 'Filter plugins to execute', collect, [ ])
.option('--no-recurse', 'Do not recurse paths')
.option('-t, --token', 'Access token (or use ACCESS_TOKEN in env)')
.option('-d, --debug', 'Enable debug')
.option('-c, --config <file>', 'Configuration file', path.join(process.cwd(), 'config'))
.option('--dry-run', 'Do not make any modifications')
.option('--exclude-all', 'Exclude all projects by default', false)
.option('--exclude <rules>', 'Exclude rule (json)', jsonCollect, [])
.option('--include <rules>', 'Include rule (json)', jsonCollect, [])
.option('--dump-projects', 'dump project configuration (and do nothing else)', false);

cmd.parse(process.argv);
const opts = /** @type {GitConfig.EnvOpts} */ (cmd.opts());

const env = new Env();

Promise.resolve()
// eslint-disable-next-line complexity
.then(async () => {
  await env.load(opts.config, opts);

  const paths = env.config.path;

  /** @type {GitConfig.$ProjectSchema[]} */
  let projects = [];
  if (env.opts.recurse) {
    for (let i = 0; i < paths.length; ++i) {
      const path = paths[i];
      env.spin.start('Listing groups in ' + path);
      const subgroups = /** @type {GroupSchema[]} */ (await env.gitlab.Groups.allSubgroups(path));
      forEach(subgroups, (desc) => paths.push(get(desc, [ 'full_path' ]))); // jshint ignore:line

      env.spin.start('Listing projects in ' + path);
      const newProjects = /** @type {GitConfig.$ProjectSchema[]} */(
        /** @type {unknown} */ (await env.gitlab.Groups.allProjects(path)));
      projects = concat(projects, filter(newProjects,
        (p) => !isIgnored(env, p, opts, 'global config'))); // jshint ignore:line
    }
  }
  env.spin.debug('Projects:', map(projects, 'name'));

  for (const project of projects) {
    if (opts.dumpProjects) {
      env.spin.info(JSON.stringify(project, undefined, 2));
    }
    else {
      for (const settings of env.config.settings) {
        const plugin = env.plugins[settings?.plugin];
        if (!plugin) {
          throw new Error(`unknown plugin: ${settings?.plugin}`);
        }
        else if ((env.opts.plugin?.length ?? 0) !== 0 &&
                 !env.opts.plugin.includes(settings?.plugin)) {
          /* plugin filter from cmdline */
          continue;
        }
        await plugin(env, project, settings, get(settings, 'service'));
      }
    }
  }
})
.then(
  () => env.spin.succeed('done'),
  (err) => {
    env.spin.fail("Error: " + err);
    if (err?.cause?.request) {
      /** @type {Request} */
      const request = err.cause.request;
      env.spin.fail("  Request: %s %s",
        request.method?.toString?.() ?? "?",
        request.url?.toString?.() ?? "?");
    }
    env.spin.debug(err?.stack);
  }
)
.finally(() => {
  // waiting for this merge: https://github.com/sindresorhus/ky/pull/145
  process.exit(0); // eslint-disable-line
});
