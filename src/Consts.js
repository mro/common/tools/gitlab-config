//@ts-check

export const AccessLevel = {
  none: /** @type {GitConfig.$AccessLevel} */ (0),
  guest: /** @type {typeof GitConfig.$AccessLevel.GUEST} */ (10),
  reporter: /** @type {typeof GitConfig.$AccessLevel.REPORTER} */ (20),
  developer: /** @type {typeof GitConfig.$AccessLevel.DEVELOPER} */ (30),
  maintainer: /** @type {typeof GitConfig.$AccessLevel.MAINTAINER} */ (40),
  owner: /** @type {typeof GitConfig.$AccessLevel.OWNER} */ (50)
};
