//@ts-check
import { wrap } from 'lodash-es';
import util from 'node:util';

import ora from 'ora';

/*  */
/**
 * Allows to use ora with variadic arguments
 * @template T=OraExt.$OraExt
 * @this {any}
 * @param {(message: string) => T} func
 * @param {string} message
 * @param {any[]} args
 * @return {T}      [description]
 */
function format(func, message, ...args) {
  return func.call(this, util.format(message, ...args)); // jshint ignore:line
}

/* Ora uses a factory, so can't directly modify prototype */
const _ora /*: typeof oraExt */ = wrap(ora, function(o, ...args) {
  var spin = /** @type {OraExt.$OraExt} */ (/** @type {any} */ (o(...args)));
  spin.start = wrap(spin.start, format);
  spin.succeed = wrap(spin.succeed, format);
  spin.fail = wrap(spin.fail, format);
  spin.warn = wrap(spin.warn, format);
  spin.info = wrap(spin.info, format);
  spin.debug = function(message, ...args) {
    if (this.options.debug) {
      this.stopAndPersist({ text: util.format(message, ...args), symbol: '\u{1f41b}' }); // jshint ignore:line
    }
    return this;
  };
  return spin;
});

export default _ora;
