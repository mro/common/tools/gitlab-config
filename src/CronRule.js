// @ts-check

/**
 * @param {string} value
 * @returns {string|Number}
 */
function toNum(value) {
  return (/^\d+$/.test(value)) ? Number(value) : value;
}

class CronRule {
  /**
   * @param {string} rule
   */
  constructor(rule) {
    this.items = rule?.split?.(' ', 5).map(toNum);
    if (this.items?.length !== 5) {
      throw new Error(`failed to parse Cron rule: ${rule}`);
    }
  }

  get minute() {
    return this.items[0];
  }

  set minute(value) {
    this.items[0] = value;
  }

  get hour() {
    return this.items[1];
  }

  set hour(value) {
    this.items[1] = value;
  }

  get dayOfMonth() {
    return this.items[2];
  }

  set dayOfMonth(value) {
    this.items[2] = value;
  }

  get month() {
    return this.items[3];
  }

  set month(value) {
    this.items[3] = value;
  }

  get dayOfWeek() {
    return this.items[4];
  }

  set dayOfWeek(value) {
    this.items[4] = value;
  }

  /**
   * @returns {string}
   */
  toRule() {
    return `${this.minute} ${this.hour} ${this.dayOfMonth} ${this.month} ${this.dayOfWeek}`;
  }

  /**
   * @param {boolean} [strict=false]
   * @returns {CronRule}
   */
  // eslint-disable-next-line complexity
  normalize(strict = false) {
    if (typeof this.minute === 'number' && this.minute >= 60) {
      if (typeof this.hour === 'number') {
        this.hour += Math.trunc(this.minute / 60);
      }
      else if (strict) {
        throw new Error(`failed to normalize ${this.toRule()}`);
      }
      this.minute %= 60;
    }
    if (typeof this.hour === 'number' && this.hour >= 24) {
      if (typeof this.dayOfMonth === 'number') {
        this.dayOfMonth += Math.trunc(this.hour / 24);
      }
      else if (typeof this.dayOfWeek === 'number') {
        this.dayOfWeek += Math.trunc(this.hour / 24);
      }
      else if (strict) {
        throw new Error(`failed to normalize ${this.toRule()}`);
      }
      this.hour %= 24;
    }
    if (typeof this.dayOfMonth === 'number' && this.dayOfMonth > 31) {
      this.dayOfMonth = (this.dayOfMonth % 31) + 1;
    }
    if (typeof this.month === 'number' && this.month > 12) {
      this.month = (this.month % 12) + 1;
    }
    if (typeof this.dayOfWeek === 'number' && this.dayOfWeek >= 7) {
      this.dayOfWeek %= 7;
    }
    return this;
  }

  /**
   * @param {CronRule} rule
   * @param {boolean} strict silently discard errors
   * @returns {CronRule}
   */
  increment(rule, strict = false) {
    for (let idx = 0; idx < this.items.length; ++idx) {
      if (typeof (rule?.items?.[idx]) !== 'number') {
        continue;
      }
      else if (typeof this.items[idx] === 'number') {
        // @ts-ignore
        this.items[idx] += rule.items[idx];
      }
      else if (strict && rule.items[idx] !== 0) {
        throw new Error(`can't add ${rule?.toRule()} to ${this.toRule()}`);
      }
    }
    this.normalize(strict);
    return this;
  }
}

export default CronRule;
