
export = GitConfig
export as namespace GitConfig

import type { GroupSchema, AccessLevel, BadgeSchema,
  ProtectedBranchAccessLevel, ProtectedBranchSchema, ProjectSchema } from '@gitbeaker/core';

export { default as Env } from './env.js';

declare namespace GitConfig {
  type $ResourceId = number
  type $GroupSchema = GroupSchema
  type $ProjectId = $ResourceId
  type $GroupId = $ResourceId
  type $UserId = $ResourceId
  export type $AccessLevel = AccessLevel
  export const $AccessLevel = AccessLevel

  type $ProtectedBranchAccessLevel = ProtectedBranchAccessLevel
  type $ProtectedBranchSchema = ProtectedBranchSchema

  type $ProjectSchema = ProjectSchema;
  type $BadgeSchema = BadgeSchema;
  type $DeployKeySchema = {
    id: number,
    title: string,
    key: string,
    created_at: string,
    can_push: boolean
  }
  type $UserSchema = {
    id: $UserId
    name: string
    username: string
    state: string
    avatar_url?: string
    web_url: string
  }
  type $MemberSchema = $UserSchema & {
    expires_at?: string,
    access_level: number
  }
  type $MergeRequestSchema = {
    id: $ResourceId,
    iid: $ResourceId,
    project_id: $ProjectId,
    title: string,
    description: string,
    state: string,
    merged_by?: $UserSchema,
    merged_at?: string,
    closed_by?: $UserSchema,
    closed_at?: string,
    created_at: string,
    updated_at: string,
    target_branch: string,
    source_branch: string,
    upvotes: number,
    downvotes: number,
    author: $UserSchema,
    assignee?: $UserSchema,
    assignees: Array<$UserSchema>,
    source_project_id: number,
    target_project_id: number,
    labels: Array<string>,
    work_in_progress: boolean,
    milestone: any,
    merge_when_pipeline_succeeds: boolean,
    merge_status: string,
    sha: string,
    merge_commit_sha: string|null,
    user_notes_count: number,
    discussion_locked: any,
    should_remove_source_branch: boolean,
    force_remove_source_branch: boolean,
    web_url: string,
    time_stats?: {
      time_estimate: number,
      total_time_spent: number,
      human_time_estimate?: string,
      human_total_time_spent?: string
    },
    squash: boolean,
    task_completion_status: any
  }
  type $MergeApprovalSchema = {
    id: $ResourceId,
    iid: $ResourceId,
    project_id: $ProjectId,
    title: string,
    description: string,
    state: string,
    created_at: string,
    updated_at: string,
    merge_status: string,
    approvals_required: number,
    approvals_left: number,
    approved_by: Array<$UserSchema>,
    approvers: Array<$UserSchema>,
    approver_groups: Array<any>
  }
  type $AccessLevelSchema = {
    access_level?: number
    user_id?: number|null
    group_id?: number|null
    access_level_description: string
  }
  type $ProtectedResourceSchema = {
    name: string
    create_access_levels: $AccessLevelSchema[]
  }
  type $PipelineScheduleSchema = {
    id: $ResourceId,
    description: string,
    ref: string,
    cron: string,
    cron_timezone: string,
    active: boolean,
    owner: {
      name: string,
      username: string,
      id: $UserId
    }
  }

  interface EnvOpts extends Filter {
    url: string,
    path?: string[],
    plugin: string[],
    token?: string|null, /* set to null to disable authentication */
    debug: boolean,
    dryRun: boolean,
    recurse: boolean,
    config: string,
    dumpProjects: boolean
  }

  interface Filter {
    include?: Array<string|{ [key:string]: any }>
    exclude?: Array<string|{ [key:string]: any }>
    excludeAll?: boolean
  }

  interface Settings extends Filter {
    plugin: string
    config?: { [id: string]: any },
  }

  interface Config {
    path: string[],
    settings: Settings[]
  }

  type Plugin = (env: Env, project: $ProjectSchema, settings: Settings, service?: string) => Promise<any>
}
