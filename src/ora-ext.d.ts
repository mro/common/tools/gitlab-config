
import type { Options } from 'ora';

export class $OraExt {
  start(fmt?: string, ...args: Array<any>): $OraExt
  succeed(fmt?: string, ...args: Array<any>): $OraExt
  fail(fmt?: string, ...args: Array<any>): $OraExt
  warn(fmt?: string, ...args: Array<any>): $OraExt
  info(fmt?: string, ...args: Array<any>): $OraExt
  debug(fmt: string, ...args: Array<any>): $OraExt
  stop(): $OraExt
  stopAndPersist(opts: { symbol?: string, text?: string, prefixText?: string }): $OraExt
  clear(): $OraExt
  render(): $OraExt
  frame(): $OraExt

  promise<T>(action: Promise<T>, options?: {}): Promise<T>

  text: string
  prefixText: string
  color: string
  spinner: string
  indent: number
  isSpinning: bool
  options: $OraOptions
}

export type $OraOptions = {
  text?: string
  prefixText?: string
  spinner?: string|{}
  color?: string
  hideCursor?: bool
  indent?: number
  interval?: number
  stream?: any
  isEnabled?: bool
  debug?: bool
}
declare function OraExtConstruct(options?: $OraOptions|string): $OraExt;

export default OraExtConstruct;
export as namespace OraExt;